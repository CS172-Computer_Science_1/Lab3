#include <iostream>
#include <string>
#include <iomanip>
using namespace std;

namespace nsEx08{
	int intEx08(){
		string strCo1, strCo2, fColor;

		cout << "####Mixing 2 Colors####\n";
		while (strCo1 != "red" || strCo1 != "blue" || strCo1 != "yellow" || strCo1 == "\n"){ 
			cout << "Enter red, blue, or yellow): ";
			cin >> strCo1;

			if (strCo1 == "red" || strCo1 == "blue" || strCo1 == "yellow"){ 

				while (strCo2 != "red" || strCo2 != "blue" || strCo2 != "yellow"){  
					cout << "Enter 2nd Color (red, blue, or yellow): ";
					cin >> strCo2;

					if (strCo2 == "red" || strCo2 == "blue" || strCo2 == "yellow"){  
						if (strCo1 == "red" && strCo2 == "blue" || strCo1 == "blue" && strCo2 == "red"){
							fColor = "purple";
							cout << "\nWhen you mix " << strCo1 << "> and <" << strCo2 << ", you get " << fColor << "  ***\n";
							cin.get();
							cin.ignore();
							return 0;  
						}
						else if (strCo1 == "red" && strCo2 == "yellow" || strCo1 == "yellow" && strCo2 == "red"){
							fColor = "orange";
							cout << "\nWhen you mix " << strCo1 << "> and <" << strCo2 << ", you get " << fColor << "  ***\n";
							cin.get();
							cin.ignore();
							return 0; 
						}
						else if (strCo1 == "blue" && strCo2 == "yellow" || strCo1 == "yellow" && strCo2 == "blue"){ //if mix is green do...
							fColor = "green";
							cout << "\nWhen you mix " << strCo1 << "> and <" << strCo2 << ", you get " << fColor << "  ***\n";
							cin.get();
							cin.ignore();
							return 0; 
						}
						else if (strCo1 == strCo2){ //Error Message it it is same color
							cout << "\n  /!\\ERROR/!\\ \n\n When You Mix <" << strCo2 << "> With <" << strCo2
								<< "> You Get <" << strCo2 << ">. \n Try Again!\n\n";						}
					}
					else{
						cout << "You did not enter red, blue, or yellow.\n\n"; //Error message on color 1
					};
				}
			}
			else{
				cout << "You did not enter red, blue, or yellow.\n\n"; //error message on color 2
			};
		}
		return 0;
	}
}