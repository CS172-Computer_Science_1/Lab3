#include <iostream>
#include <string>
#include "lab3_ex01.h"  
#include "lab3_ex08.h"
#include "lab3_ex23.h"

using namespace std;

int main(){
	for (int iFor = 0; iFor < 10; iFor++){ //<-Prevent Infinite Loop
		char chChoice = '-';
		
		cout << ".:[Pick an exercise (1, 8, or 23)]:.\n\n";
		cout << "(1) Lab 3, Exercise 01.\n"
			<< "(2) Lab 3, Exercise 08.\n"
			<< "(3) Lab 3, Exercise 23.\n";

		cin >> chChoice;
		cout << "You selected: " << chChoice << endl;

		switch (chChoice){
		case '1':
			cout << "\n####Lab 3, Exercise 1####\n";
			nsEx01::intEx01();
			break;
		case '2':
			cout << "\n####Lab 3, Exercise 8####\n";
			nsEx08::intEx08();
			break;
		case '3':
			cout << "\n####Lab 3, Exercise 23####\n";
			nsEx23::intEx23();
			break;
		default:
			cout << "Try again!!! \n\n";
			break;
			cin.ignore();  //Prevent Letter Loop
		}
	}
}