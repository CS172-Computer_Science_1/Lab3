#include <iostream>
using namespace std;

namespace nsEx01{
	int intEx01(){
		int nr1, nr2;
		cout << "Calculator to check which number is larger\n";
		cout << "Enter 1st Number: ";
		cin >> nr1;
		cout << "Enter 1st number: ";
		cin >> nr2;
		cout << endl;

		(nr1 == nr2) ? (cout << "Its the same number, try again\n") : (
			(nr1 > nr2) ? (cout << "The 1st number <" << nr1 << "> is larger than the 2nd <" << nr2 << ">\n\n") :
			(cout << "The 2nd number <" << nr2 << "> is larger than the 1st <" << nr1 << ">\n\n")
			);

		cin.get();
		cin.ignore();
		return 0;
	}
}