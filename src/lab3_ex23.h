#include <iostream>
#include <string>
#include <cmath>
#include <sstream>
using namespace std;

namespace nsEx23{
	int intEx23(){
		double dblPi = 3.14159, dblR, dblArea, dblLength, dblWidth, dblBase, dblHeight;
		char chChoice = '0';

		cout << "Area Calculator" << "\n\n";
		cout << "1. Area of a Circle.\n"
			<< "2. Area of a Rectangle.\n"
			<< "3. Area of a Triangle.\n"
			<< "4. Quit\n\n";
		cout << "Enter your choice from 1 to 4: ";
		cin >> chChoice;

		switch (chChoice){
		case '1':
			cout << "\nEnter Radius: ";
			cin >> dblR;

			if (dblR > 0){
				dblArea = dblPi*pow(dblR, 2);
				cout << "The Area of the Circle is: " << dblArea << endl << endl;
				cin.get();
				cin.ignore();
				return 0;
			}
			else cout << "Not a Positive Number";
			cin.get();
			cin.ignore();
			break;
		case'2':
			cout << "\nEnter Rectangle Length: ";
			cin >> dblLength;
			cout << "Enter Rectangle Width: ";
			cin >> dblWidth;

			if (dblLength > 0 && dblWidth > 0){
				dblArea = dblLength * dblWidth;
				cout << "The Area of the Circle is: " << dblArea << endl << endl;
				cin.get();
				cin.ignore();
				return 0;
			}
			else cout << "Not a Positive Number";
			cin.get();
			cin.ignore();
			break;
		case'3':
			cout << "\nEnter Triangle Base: ";
			cin >> dblBase;
			cout << "Enter Triangle Height: ";
			cin >> dblHeight;

			if (dblBase > 0 && dblHeight > 0){
				dblArea = dblBase * dblHeight * 0.5;
				cout << "The Area of the Circle is: " << dblArea << endl << endl;
				cin.get();
				cin.ignore();
				return 0;
			}
			else cout << " Not a Positive Number ";
			cin.get();
			cin.ignore();
			break;
		case'4':
			break;
		default:
			cin.get();
			cin.ignore();
			cout << "/!\\Invalid Answer/!\\, Try Again\n\n";
			break;
		}
		return 0;
	}
}